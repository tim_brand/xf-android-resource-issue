﻿using System;

using Xamarin.Forms;
using FFImageLoading.Forms;

namespace android_resource_issue
{
    public class App : Application
    {
        public App ()
        {
            // The root page of your application
            var content = new ContentPage {
                Title = "android_resource_issue",
                Content = new StackLayout {
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        new Label {
                            HorizontalTextAlignment = TextAlignment.Center,
                            Text = "Welcome to Xamarin Forms!"
                        },
                        new CachedImage {
                            Source = "https://blog.xamarin.com/wp-content/uploads/2015/03/RDXWoY7W_400x400.png"
                        }
                    }
                }
            };

            MainPage = new NavigationPage (content);
        }

        protected override void OnStart ()
        {
            // Handle when your app starts
        }

        protected override void OnSleep ()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume ()
        {
            // Handle when your app resumes
        }
    }
}

